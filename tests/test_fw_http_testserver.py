"""Test HttpTestServer."""

import flask
import requests

pytest_plugins = "fw_http_testserver"
timeout = {"timeout": 0.1}


def test_http_testserver_add_response(http_testserver):
    callback_called = False

    def callback(request):
        assert request.name == "GET /path"
        nonlocal callback_called
        callback_called = True

    http_testserver.add_response("/path", {"foo": "bar"}, func=callback)

    response = requests.get(f"{http_testserver.url}/path", **timeout)
    expected_request = {
        "name": "GET /path",
        "method": "GET",
        "url": "/path",
        "params": {},
        "headers": {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate",
            "Connection": "keep-alive",
            "Host": http_testserver.addr,
            "User-Agent": f"python-requests/{requests.__version__}",
        },
        "form": {},
        "body": b"",
        "json": None,
        "files": {},
    }

    assert response.ok
    assert response.json() == {"foo": "bar"}
    assert http_testserver.request_log == ["GET /path"]
    assert http_testserver.request_map == {"GET /path": expected_request}
    assert http_testserver.requests == [expected_request]
    assert http_testserver.pop_first() == expected_request
    assert http_testserver.requests == []
    assert callback_called


def test_http_testserver_file_form(http_testserver):
    http_testserver.add_response("/upload", method="POST")
    files = {"file1.txt": b"foo", "file2.txt": b"bar"}

    response = requests.post(f"{http_testserver.url}/upload", files=files, **timeout)

    assert response.ok
    assert http_testserver.request_log == ["POST /upload"]
    request = http_testserver.requests[0]
    assert request.files == files
    assert request.body


def test_http_testserver_proxies_flask_request(http_testserver):
    assert hasattr(http_testserver, "request")
    assert http_testserver.request is flask.request


def test_http_testserver_override_endpoint(http_testserver):
    http_testserver.add_response("/path", "foo")
    response = requests.get(f"{http_testserver.url}/path", **timeout)
    assert response.text == "foo"
    http_testserver.add_response("/path", "bar")
    response = requests.get(f"{http_testserver.url}/path", **timeout)
    assert response.text == "bar"


def test_http_testserver_repr(http_testserver):
    assert str(http_testserver) == f"HttpTestServer(port={http_testserver.port})"
