# fw-http-testserver

HTTP test server for stubbing / mocking 3rd party services.

## Installation

Add as a `poetry` dev dependency to your project:

```bash
poetry add --dev fw-http-testserver
```

## Usage

```python
import requests
pytest_plugins = "fw_http_testserver"

def test_my_app(http_testserver):
    http_testserver.add_response("/path", {"foo": "bar"})
    resp = requests.get(f"{http_testserver.url}/path")
    assert resp.json() == {"foo": "bar"}
```

## Development

Install the project using `poetry` and enable `pre-commit`:

```bash
poetry install
pre-commit install
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
